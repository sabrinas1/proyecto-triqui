var turno = "X"


//Función que crea los componentes del tablero
function crearTablero() {
    let miBoton;
    let formulario;

    formulario = document.getElementById("tablero");

    for (let i = 1; i < 10; i++) {
        miBoton = document.createElement("input");
        miBoton.setAttribute("type", "button");
        miBoton.setAttribute("class", "casilla");
        miBoton.setAttribute("id", "boton" + i);
        miBoton.setAttribute("value", " ");
        miBoton.setAttribute("onclick", "realizarJugada(this)");
        formulario.appendChild(miBoton);
        if (i % 3 == 0) {
            let salto = document.createElement("br")
            formulario.appendChild(salto);
        }
    }
}

// Función que realiza la jugada 
function realizarJugada(boton) {
    if (boton.value != " ") {
        //casilla no vacia
        alert('Esa casilla ya está ocupada. Elija otra')
    } else {
        boton.value = turno;
        let victoria = compruebaVictoria(turno);
        if (victoria) {
            medallasGanador =  document.getElementById("medallas"+ turno);
            medallasGanador.innerHTML = medallasGanador.innerHTML + "🥇";
            alert('Ha ganado el jugador... ' + turno);
            nuevoJuego();
        } else {
            let empate = compruebaEmpate();
            if (empate) {
                medallasGanador1 =  document.getElementById("medallasX");
                medallasGanador2 =  document.getElementById("medallasO");
                medallasGanador1.innerHTML = medallasGanador1.innerHTML + "🥈";
                medallasGanador2.innerHTML = medallasGanador2.innerHTML + "🥈";
                alert('Ha sido empate, cada quien gana una medalla de plata ');
                nuevoJuego();
            } else {
                if (turno == "X") {
                    turno = "O"
                } else {
                    turno = "X"
                }
            }
        }
    }
}


// Función que comprueba si algua persona ha ganado
function compruebaVictoria(ficha) {

    let tablero = document.getElementById("tablero");
    var x
    for (x = 0; x < 9; x += 3) {
        //comprueba las filas
        if ((tablero[x].value == ficha) && (tablero[x + 1].value == ficha) && (tablero[x + 2].value == ficha))
            return true;
    }
    for (x = 0; x < 3; x++) {
        //comprueba las columnas
        if ((tablero[x].value == ficha) && (tablero[x + 3].value == ficha) && (tablero[x + 6].value == ficha))
            return true;
    }
    //comprueba las diagonales
    if ((tablero[2].value == ficha) && (tablero[4].value == ficha) && (tablero[6].value == ficha))
        return true;
    if ((tablero[0].value == ficha) && (tablero[4].value == ficha) && (tablero[8].value == ficha))
        return true;
    return false;
}

function compruebaEmpate() {
    for (x = 0; x < 9; x++) {
        //comprueba todos los elementos
        if ((tablero[x].value == " "))
            return false;
    }
    return true;
}

function nuevoJuego(){
    for (x = 0; x < 9; x++) {
        tablero[x].value = " ";
    }
}
 
function limpiarTodo(){
    medallasGanador1 =  document.getElementById("medallasX");
    medallasGanador2 =  document.getElementById("medallasO");
    medallasGanador1.innerHTML = " ";
    medallasGanador2.innerHTML = " ";

    nuevoJuego();
}
